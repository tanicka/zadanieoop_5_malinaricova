#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::EffectBlackandWhite()
{
	paintWidget.blackandwhite();
}

void MyPainter::EffectNegativ()
{
	paintWidget.negativ();
}

void MyPainter::EffectRotate_Right()
{
	paintWidget.rotate_right();
}

void MyPainter::EffectRotate_Left()
{
	paintWidget.rotate_left();
}



void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}
