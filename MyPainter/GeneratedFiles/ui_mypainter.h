/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionRotate;
    QAction *actionNegative;
    QAction *actionRotate_2;
    QAction *actionBlack_and_White_2;
    QAction *actionNegativ;
    QAction *actionBlackandWhite;
    QAction *actionRotate_Right;
    QAction *actionRotate_Left;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(584, 494);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionRotate = new QAction(MyPainterClass);
        actionRotate->setObjectName(QStringLiteral("actionRotate"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionRotate_2 = new QAction(MyPainterClass);
        actionRotate_2->setObjectName(QStringLiteral("actionRotate_2"));
        actionBlack_and_White_2 = new QAction(MyPainterClass);
        actionBlack_and_White_2->setObjectName(QStringLiteral("actionBlack_and_White_2"));
        actionNegativ = new QAction(MyPainterClass);
        actionNegativ->setObjectName(QStringLiteral("actionNegativ"));
        actionBlackandWhite = new QAction(MyPainterClass);
        actionBlackandWhite->setObjectName(QStringLiteral("actionBlackandWhite"));
        actionRotate_Right = new QAction(MyPainterClass);
        actionRotate_Right->setObjectName(QStringLiteral("actionRotate_Right"));
        actionRotate_Left = new QAction(MyPainterClass);
        actionRotate_Left->setObjectName(QStringLiteral("actionRotate_Left"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 554, 403));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);


        verticalLayout->addLayout(horizontalLayout);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 584, 31));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);
        menuEffects->addAction(actionNegativ);
        menuEffects->addAction(actionBlackandWhite);
        menuEffects->addAction(actionRotate_Right);
        menuEffects->addAction(actionRotate_Left);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(menuBar, SIGNAL(triggered(QAction*)), MyPainterClass, SLOT(EffectBlack_White()));
        QObject::connect(actionNegativ, SIGNAL(triggered()), MyPainterClass, SLOT(EffectNegativ()));
        QObject::connect(actionBlackandWhite, SIGNAL(triggered()), MyPainterClass, SLOT(EffectBlackandWhite()));
        QObject::connect(actionRotate_Right, SIGNAL(triggered()), MyPainterClass, SLOT(EffectRotate_Right()));
        QObject::connect(actionRotate_Left, SIGNAL(triggered()), MyPainterClass, SLOT(EffectRotate_Left()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionRotate->setText(QApplication::translate("MyPainterClass", "Rotate", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionRotate_2->setText(QApplication::translate("MyPainterClass", "RotateRight", 0));
        actionBlack_and_White_2->setText(QApplication::translate("MyPainterClass", "BlackandWhite", 0));
        actionNegativ->setText(QApplication::translate("MyPainterClass", "Negativ", 0));
        actionBlackandWhite->setText(QApplication::translate("MyPainterClass", "BlackandWhite", 0));
        actionRotate_Right->setText(QApplication::translate("MyPainterClass", "Rotate_Right", 0));
        actionRotate_Left->setText(QApplication::translate("MyPainterClass", "Rotate_Left", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
