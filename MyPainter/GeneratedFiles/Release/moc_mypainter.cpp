/****************************************************************************
** Meta object code from reading C++ file 'mypainter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mypainter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mypainter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MyPainter_t {
    QByteArrayData data[10];
    char stringdata0[126];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MyPainter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MyPainter_t qt_meta_stringdata_MyPainter = {
    {
QT_MOC_LITERAL(0, 0, 9), // "MyPainter"
QT_MOC_LITERAL(1, 10, 10), // "ActionOpen"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 10), // "ActionSave"
QT_MOC_LITERAL(4, 33, 11), // "EffectClear"
QT_MOC_LITERAL(5, 45, 9), // "ActionNew"
QT_MOC_LITERAL(6, 55, 13), // "EffectNegativ"
QT_MOC_LITERAL(7, 69, 19), // "EffectBlackandWhite"
QT_MOC_LITERAL(8, 89, 18), // "EffectRotate_Right"
QT_MOC_LITERAL(9, 108, 17) // "EffectRotate_Left"

    },
    "MyPainter\0ActionOpen\0\0ActionSave\0"
    "EffectClear\0ActionNew\0EffectNegativ\0"
    "EffectBlackandWhite\0EffectRotate_Right\0"
    "EffectRotate_Left"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MyPainter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    0,   58,    2, 0x0a /* Public */,
       7,    0,   59,    2, 0x0a /* Public */,
       8,    0,   60,    2, 0x0a /* Public */,
       9,    0,   61,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MyPainter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MyPainter *_t = static_cast<MyPainter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ActionOpen(); break;
        case 1: _t->ActionSave(); break;
        case 2: _t->EffectClear(); break;
        case 3: _t->ActionNew(); break;
        case 4: _t->EffectNegativ(); break;
        case 5: _t->EffectBlackandWhite(); break;
        case 6: _t->EffectRotate_Right(); break;
        case 7: _t->EffectRotate_Left(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MyPainter::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MyPainter.data,
      qt_meta_data_MyPainter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MyPainter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MyPainter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MyPainter.stringdata0))
        return static_cast<void*>(const_cast< MyPainter*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MyPainter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
